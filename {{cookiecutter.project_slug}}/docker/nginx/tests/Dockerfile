ARG sourceimage="{{ cookiecutter.project_slug }}-nginx"

FROM ${sourceimage}

ARG gitlab_host="codebase.helmholtz.cloud"
ARG gitlab_bats="hcdc/bats-core"

ARG bats_core_ref="v1.9.0"
ARG bats_support_ref="v0.3.0"
ARG bats_assert_ref="v2.1.0"

# install bats from fork at https://codebase.helmholtz.cloud/hcdc/bats-core
RUN mkdir -p tests/bats \
  tests/test_helper/bats-assert \
  tests/test_helper/bats-support

RUN curl https://${gitlab_host}/${gitlab_bats}/bats-core/-/archive/${bats_core_ref}/bats-core-${bats_core_ref}.tar.gz | \
  tar xz --strip-components 1 -C tests/bats

RUN curl https://${gitlab_host}/${gitlab_bats}/bats-support/-/archive/${bats_support_ref}/bats-support-${bats_support_ref}.tar.gz | \
  tar xz --strip-components 1 -C tests/test_helper/bats-support

RUN curl https://${gitlab_host}/${gitlab_bats}/bats-assert/-/archive/${bats_assert_ref}/bats-assert-${bats_assert_ref}.tar.gz | \
  tar xz --strip-components 1 -C tests/test_helper/bats-assert

# add test files
ADD . tests

CMD ["./tests/bats/bin/bats", "tests"]
