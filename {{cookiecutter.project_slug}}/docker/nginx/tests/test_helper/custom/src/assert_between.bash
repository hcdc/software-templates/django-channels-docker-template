# assert_between
# ==============
#
# Summary: Fail if the number in the first statement is not within certain limits
#
# Usage: assert_between <actual> <lower-limit> <upper-limit>
#
# Options:
#   <actual>      The value to evaulate (any integer is extracted here and tested) (note: floating point numbers are not supported!)
#   <lower-limit>    The lower limit of the test interval.
#   <upper-limit>    The upper limit of the test interval.
#
#   ```bash
#   @test 'assert_between()' {
#     assert_between 'some_value 60' '59' '61'
#   }
#   ```
#
# IO:
#   STDERR - reference and actual values, on failure
# Globals:
#   none
# Returns:
#   0 - if the number in the actual value is within the given limits
#   1 - otherwise
#
# On failure, the limits and actual values are displayed.
#
#   ```
#   -- actual is smaller than reference --
#   lower limit : 59
#   upper limit : 61
#   actual      : 60
#   full text   : some_value 60
#   --
#   ```
assert_between() {
  for NUMBER in $(echo "$1" | grep -Eo '(-)?[0-9]+'); do
    if ! (( "$NUMBER" <= "$3" )) && (( "$NUMBER" >= "$2" )); then
      batslib_print_kv_single_or_multi 8 \
      'lower limit' "$2" \
      'upper limit' "$3" \
      'actual'   "$NUMBER" \
      'full text'   "$1" \
      | batslib_decorate 'actual number is not within the limits' \
      | fail
    fi
  done
}
